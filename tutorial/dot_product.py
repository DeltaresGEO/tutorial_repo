from scipy.sparse import csc_matrix


def dot_product(A, b):
    """
    Calculates the dot product of A dot b

    :param A: sparse matrix
    :param b: floating value
    :return:
    """
    return A.dot(b)


if __name__ == '__main__':
    A = csc_matrix([[1, 0],
                    [0, 4]])
    b = 5.

    print(dot_product(A, b))





