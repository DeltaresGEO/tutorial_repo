from tutorial.dot_product import dot_product
from scipy.sparse import csc_matrix
import pytest


def test_dot_product():
    # set test data
    A = csc_matrix([[1, 0], [0, 4]])
    b = 5.

    # calculate
    F = dot_product(A, b)

    # assert
    assert F[0, 0] == pytest.approx(5)
    assert F[1, 1] == pytest.approx(20)
